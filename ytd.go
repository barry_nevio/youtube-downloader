package ytd

import (
	"fmt"
	"html"
	"net/url"
	"os"
	"regexp"
	"strings"
	"time"

	uuid "github.com/satori/go.uuid"
	"github.com/spf13/cast"

	"github.com/cavaliercoder/grab"
	"gitlab.com/barry_nevio/goo-tools/Error"
	"gitlab.com/barry_nevio/goo-tools/StringThings"

	"gitlab.com/barry_nevio/youtube-downloader/models/VideoInfo"
)

// DownloadInfoModel ... A model storing the info of the completed download
type DownloadInfoModel struct {
	VideoFileName string
	AudioFileName string
	YTVideoID     string
	Path          string
	DownloadVideo bool // ! ONLY WORKS FOR ADAPTIVE STREAMS
	DownloadAudio bool // ! ONLY WORKS FOR ADAPTIVE STREAMS
}

// DownloadFromIDToPath ...
func DownloadFromIDToPath(downloadInfoModel DownloadInfoModel) (DownloadInfoModel, error) {

	// Get the video info
	videoInfo, err := VideoInfo.GetNewFromID(downloadInfoModel.YTVideoID)
	if err != nil {
		return downloadInfoModel, err
	}

	// From the video info, grab the best option for downloading
	formatDecisionModel := videoInfo.GetBestDownloadOption()

	// Nothing we can use right now
	if !formatDecisionModel.UseAdaptiveFormatStreams &&
		!formatDecisionModel.UseFormatStream &&
		!formatDecisionModel.UseEmbededStream {
		return downloadInfoModel, Error.New("THERE ARE NO DOWNLOAD STREAMS AVAILABLE FOR THIS VIDEO.")
	}

	// Use adaptive stream
	if formatDecisionModel.UseAdaptiveFormatStreams {
		// Download Video
		if downloadInfoModel.DownloadVideo {
			adaptiveVideoDownloadInfoModel, err := downloadAdaptiveVideoFormatToPath(formatDecisionModel, downloadInfoModel.Path)
			if err != nil {
				return downloadInfoModel, err
			}
			downloadInfoModel.VideoFileName = adaptiveVideoDownloadInfoModel.VideoFileName
		}
		// Download Audio
		if downloadInfoModel.DownloadAudio {
			adaptiveAudioDownloadInfoModel, err := downloadAdaptiveAudioFormatToPath(formatDecisionModel, downloadInfoModel.Path)
			if err != nil {
				return downloadInfoModel, err
			}
			downloadInfoModel.AudioFileName = adaptiveAudioDownloadInfoModel.AudioFileName
		}
		return downloadInfoModel, nil
	}

	// Use standard stream
	if formatDecisionModel.UseFormatStream {
		return downloadStandardFormatToPath(formatDecisionModel, downloadInfoModel.Path)
	}

	// Use embeded stream
	if formatDecisionModel.UseEmbededStream {
		return downloadInfoModel, Error.New("THIS VIDEO IS ONLY AVAILABLE THROUGH AN EMBEDED STREAM. THE WORK AROUND FOR THIS IS NOT YET PROGRAMMED.")
	}

	return downloadInfoModel, nil
}

// downloadAdaptiveVideoFormatToPath ...
func downloadAdaptiveVideoFormatToPath(formatDecisionModel VideoInfo.FormatDecisionModel, path string) (DownloadInfoModel, error) {

	var downloadInfoModel DownloadInfoModel
	var err error

	// We assume that there are at least 2 items in the adaptive format array

	// ** DOWNLOAD VIDEO: BEGIN ** //
	videoExt := getExtFromMimeInURL(formatDecisionModel.AdaptiveFormatStreams[0].URL)
	videoFilename := generateFilenameFromTitle(formatDecisionModel.Title) + "_VIDEO"
	downloadInfoModel.VideoFileName = videoFilename + videoExt

	fmt.Println("DOWNLOADING ADAPTIVE STREAM VIDEO: " + downloadInfoModel.VideoFileName)
	err = downloadFromTo(formatDecisionModel.AdaptiveFormatStreams[0].URL, path+string(os.PathSeparator)+downloadInfoModel.VideoFileName)
	if err != nil {
		return downloadInfoModel, err
	}
	// ** DOWNLOAD VIDEO: END ** //

	return downloadInfoModel, nil
}

// downloadAdaptiveAudioFormatToPath ...
func downloadAdaptiveAudioFormatToPath(formatDecisionModel VideoInfo.FormatDecisionModel, path string) (DownloadInfoModel, error) {

	var downloadInfoModel DownloadInfoModel
	var err error

	// We assume that there are at least 2 items in the adaptive format array

	// ** DOWNLOAD AUDIO: BEGIN ** //
	audioExt := getExtFromMimeInURL(formatDecisionModel.AdaptiveFormatStreams[1].URL)
	audioFilename := generateFilenameFromTitle(formatDecisionModel.Title) + "_AUDIO"
	downloadInfoModel.AudioFileName = audioFilename + audioExt

	fmt.Println("DOWNLOADING ADAPTIVE STREAM AUDIO: " + downloadInfoModel.AudioFileName)
	err = downloadFromTo(formatDecisionModel.AdaptiveFormatStreams[1].URL, path+string(os.PathSeparator)+downloadInfoModel.AudioFileName)
	if err != nil {
		return downloadInfoModel, err
	}
	// ** DOWNLOAD AUDIO: END ** //

	return downloadInfoModel, nil
}

// downloadStandardFormatToPath ...
func downloadStandardFormatToPath(formatDecisionModel VideoInfo.FormatDecisionModel, path string) (DownloadInfoModel, error) {

	var downloadInfoModel DownloadInfoModel
	var err error

	// Create a filename
	videoExt := getExtFromMimeInURL(formatDecisionModel.FormatStream.URL)
	videoFilename := generateFilenameFromTitle(formatDecisionModel.Title) + "_VIDEO"
	downloadInfoModel.VideoFileName = videoFilename + videoExt

	fmt.Println("DOWNLOADING STANDARD STREAM: " + downloadInfoModel.VideoFileName)
	err = downloadFromTo(formatDecisionModel.FormatStream.URL, path+string(os.PathSeparator)+downloadInfoModel.VideoFileName)
	if err != nil {
		return downloadInfoModel, err
	}

	return downloadInfoModel, nil
}

// getExtFromMimeInURL ...
func getExtFromMimeInURL(targetURL string) string {

	parsedURL, _ := url.Parse(targetURL)
	parsedQuery, _ := url.ParseQuery(parsedURL.RawQuery)
	var mime string
	for key, val := range parsedQuery {
		if key == "mime" && len(val) > 0 {
			mime = val[0]
		}
	}
	var ext string
	mimeParts := strings.Split(mime, "/")
	if len(mimeParts) > 1 {
		ext = "." + mimeParts[1]
	}

	return ext
}

// generateFilenameFromTitle ...
func generateFilenameFromTitle(title string) string {

	var filename string
	filename = StringThings.FixNonUTF8(title)
	nonAlphaNumeric := regexp.MustCompile(`[^a-zA-Z0-9\s]+`)
	filename = nonAlphaNumeric.ReplaceAllString(filename, "")
	space := regexp.MustCompile(`\s+`)
	filename = space.ReplaceAllString(filename, "_")

	// This is for when a title is in a different language, this can save it
	uuid4, _ := uuid.NewV4()
	filename = filename + "_" + uuid4.String() + "LANG_ISSUE"

	// Make sure the filename isn't too large
	if len(filename) >= 47 {
		filename = filename[0:47]
	}

	// Tack on a time stamp too
	filename = filename + "_" + cast.ToString(time.Now().UnixNano())

	return filename
}

func downloadFromTo(targetURL string, destinationFile string) error {

	// Fix crap url
	targetURL = html.UnescapeString(targetURL)

	// create client
	client := grab.NewClient()
	req, err := grab.NewRequest(destinationFile, targetURL)
	if err != nil {
		return err
	}

	// start download
	resp := client.Do(req)

	// start UI loop
	t := time.NewTicker(time.Second)
	defer t.Stop()

Loop:
	for {
		select {
		case <-t.C:
			// Clear current console line
			fmt.Printf("\r%s", strings.Repeat(" ", 30)) // The 30 represents the length of "Downloading... 99.99% complete"
			// Replace current line with the progress
			fmt.Printf("\rDownloading... %.2f%% complete", 100*resp.Progress())

		case <-resp.Done:
			// download is complete
			// Clear current console line
			fmt.Printf("\r%s", strings.Repeat(" ", 30))
			// Replace current line with the progress
			fmt.Printf("\rDownloading... %.2f%% complete", 100*resp.Progress())
			break Loop
		}
	}

	// check for errors
	if err := resp.Err(); err != nil {
		return err
	}

	fmt.Println("\nDownload saved to: " + resp.Filename)

	return nil
}
