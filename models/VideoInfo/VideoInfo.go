package VideoInfo

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"

	"github.com/spf13/cast"
	"gitlab.com/barry_nevio/goo-tools/Error"
)

// FormatDecisionModel ... A model this package can return that contains the decisions made in this package on what streams to use and the data associated with it
type FormatDecisionModel struct {
	Title                    string
	UseAdaptiveFormatStreams bool
	AdaptiveFormatStreams    []AdaptiveFormat
	UseFormatStream          bool
	FormatStream             Format
	UseEmbededStream         bool // NOT YET PROGRAMED AND WONT BE FOR A WHILE. THIS IS THE CRAZY THING THIS GUY SUGGESTS https://tyrrrz.me/blog/reverse-engineering-youtube
}

// AdaptiveFormat .. This, like the name suggests; represents adaptive quality video formats where the video and audio streams are sperate but have the highest fidelity available
type AdaptiveFormat struct {
	Itag      int    `json:"itag"`
	URL       string `json:"url"`
	MimeType  string `json:"mimeType"`
	Bitrate   int    `json:"bitrate"`
	Width     int    `json:"width"`
	Height    int    `json:"height"`
	InitRange struct {
		Start string `json:"start"`
		End   string `json:"end"`
	} `json:"initRange"`
	IndexRange struct {
		Start string `json:"start"`
		End   string `json:"end"`
	} `json:"indexRange"`
	LastModified     string `json:"lastModified"`
	ContentLength    string `json:"contentLength"`
	Quality          string `json:"quality"`
	Fps              int    `json:"fps"`
	QualityLabel     string `json:"qualityLabel"`
	ProjectionType   string `json:"projectionType"`
	AverageBitrate   int    `json:"averageBitrate"`
	ApproxDurationMs string `json:"approxDurationMs"`
	ColorInfo        struct {
		Primaries               string `json:"primaries"`
		TransferCharacteristics string `json:"transferCharacteristics"`
		MatrixCoefficients      string `json:"matrixCoefficients"`
	} `json:"colorInfo"`
	HighReplication bool   `json:"highReplication"`
	AudioQuality    string `json:"audioQuality"`
	AudioSampleRate string `json:"audioSampleRate"`
	AudioChannels   int    `json:"audioChannels"`
}

// Format .. This, like the name suggests; represents video formats, however; unlike AdaptiveFormat, these streams have the video/audio in the same conatiner and have a better compatibility rate
type Format struct {
	Itag             int    `json:"itag"`
	URL              string `json:"url"`
	MimeType         string `json:"mimeType"`
	Bitrate          int    `json:"bitrate"`
	Width            int    `json:"width"`
	Height           int    `json:"height"`
	LastModified     string `json:"lastModified"`
	ContentLength    string `json:"contentLength"`
	Quality          string `json:"quality"`
	QualityLabel     string `json:"qualityLabel"`
	ProjectionType   string `json:"projectionType"`
	AverageBitrate   int    `json:"averageBitrate"`
	AudioQuality     string `json:"audioQuality"`
	ApproxDurationMs string `json:"approxDurationMs"`
	AudioSampleRate  string `json:"audioSampleRate"`
	AudioChannels    int    `json:"audioChannels"`
}

// Context ... The big khauna burger
type Context struct {
	InnertubeAPIKey               string  `json:"innertube_api_key"`
	GAPIHintParams                string  `json:"gapi_hint_params"`
	HostLanguage                  string  `json:"host_language"`
	FMTList                       string  `json:"fmt_list"` // Needs to be parsed
	VSSHost                       string  `json:"vss_host"`
	InnertubeContextClientVersion string  `json:"innertube_context_client_version"`
	InnertubeAPIVersion           string  `json:"innertube_api_version"`
	EnableCSI                     string  `json:"enablecsi"`
	FExp                          []int64 `json:"fexp"`
	RootVEType                    string  `json:"root_ve_type"`
	Watermark                     string  `json:"watermark"`
	CSIPageType                   string  `json:"csi_page_type"`
	Timestamp                     int64   `json:"timestamp"`
	FFlags                        string  `json:"fflags"` // Needs to be parsed
	CSN                           string  `json:"csn"`
	C                             string  `json:"c"`
	URLEncodedFMTStreamMap        string  `json:"url_encoded_fmt_stream_map"` // Needs to be parsed
	HL                            string  `json:"hl"`
	CVer                          string  `json:"cver"`
	CR                            string  `json:"cr"`
	AdaptiveFmts                  string  `json:"adaptive_fmts"` // Needs to be parsed
	PlayerResponse                struct {
		PlayabilityStatus struct {
			Status          string `json:"status"`
			PlayableInEmbed bool   `json:"playableInEmbed"`
		} `json:"playabilityStatus"`
		StreamingData struct {
			ExpiresInSeconds string           `json:"expiresInSeconds"`
			Formats          []Format         `json:"formats"`
			AdaptiveFormats  []AdaptiveFormat `json:"adaptiveFormats"`
		} `json:"streamingData"`
		PlaybackTracking struct {
			VideostatsPlaybackURL struct {
				BaseURL string `json:"baseUrl"`
			} `json:"videostatsPlaybackUrl"`
			VideostatsDelayplayURL struct {
				BaseURL string `json:"baseUrl"`
			} `json:"videostatsDelayplayUrl"`
			VideostatsWatchtimeURL struct {
				BaseURL string `json:"baseUrl"`
			} `json:"videostatsWatchtimeUrl"`
			PtrackingURL struct {
				BaseURL string `json:"baseUrl"`
			} `json:"ptrackingUrl"`
			QoeURL struct {
				BaseURL string `json:"baseUrl"`
			} `json:"qoeUrl"`
			SetAwesomeURL struct {
				BaseURL                 string `json:"baseUrl"`
				ElapsedMediaTimeSeconds int    `json:"elapsedMediaTimeSeconds"`
			} `json:"setAwesomeUrl"`
			AtrURL struct {
				BaseURL                 string `json:"baseUrl"`
				ElapsedMediaTimeSeconds int    `json:"elapsedMediaTimeSeconds"`
			} `json:"atrUrl"`
		} `json:"playbackTracking"`
		Captions struct {
			PlayerCaptionsRenderer struct {
				BaseURL    string `json:"baseUrl"`
				Visibility string `json:"visibility"`
			} `json:"playerCaptionsRenderer"`
			PlayerCaptionsTracklistRenderer struct {
				CaptionTracks []struct {
					BaseURL string `json:"baseUrl"`
					Name    struct {
						SimpleText string `json:"simpleText"`
					} `json:"name"`
					VssID          string `json:"vssId"`
					LanguageCode   string `json:"languageCode"`
					Kind           string `json:"kind"`
					IsTranslatable bool   `json:"isTranslatable"`
				} `json:"captionTracks"`
				AudioTracks []struct {
					CaptionTrackIndices []int  `json:"captionTrackIndices"`
					Visibility          string `json:"visibility"`
				} `json:"audioTracks"`
				TranslationLanguages []struct {
					LanguageCode string `json:"languageCode"`
					LanguageName struct {
						SimpleText string `json:"simpleText"`
					} `json:"languageName"`
				} `json:"translationLanguages"`
				DefaultAudioTrackIndex int `json:"defaultAudioTrackIndex"`
			} `json:"playerCaptionsTracklistRenderer"`
		} `json:"captions"`
		VideoDetails struct {
			VideoID          string `json:"videoId"`
			Title            string `json:"title"`
			LengthSeconds    string `json:"lengthSeconds"`
			ChannelID        string `json:"channelId"`
			IsOwnerViewing   bool   `json:"isOwnerViewing"`
			ShortDescription string `json:"shortDescription"`
			IsCrawlable      bool   `json:"isCrawlable"`
			Thumbnail        struct {
				Thumbnails []struct {
					URL    string `json:"url"`
					Width  int    `json:"width"`
					Height int    `json:"height"`
				} `json:"thumbnails"`
			} `json:"thumbnail"`
			AverageRating     float64 `json:"averageRating"`
			AllowRatings      bool    `json:"allowRatings"`
			ViewCount         string  `json:"viewCount"`
			Author            string  `json:"author"`
			IsPrivate         bool    `json:"isPrivate"`
			IsUnpluggedCorpus bool    `json:"isUnpluggedCorpus"`
			IsLiveContent     bool    `json:"isLiveContent"`
		} `json:"videoDetails"`
		PlayerConfig struct {
			AudioConfig struct {
				LoudnessDb              float64 `json:"loudnessDb"`
				PerceptualLoudnessDb    float64 `json:"perceptualLoudnessDb"`
				EnablePerFormatLoudness bool    `json:"enablePerFormatLoudness"`
			} `json:"audioConfig"`
			MediaCommonConfig struct {
				DynamicReadaheadConfig struct {
					MaxReadAheadMediaTimeMs int `json:"maxReadAheadMediaTimeMs"`
					MinReadAheadMediaTimeMs int `json:"minReadAheadMediaTimeMs"`
					ReadAheadGrowthRateMs   int `json:"readAheadGrowthRateMs"`
				} `json:"dynamicReadaheadConfig"`
			} `json:"mediaCommonConfig"`
		} `json:"playerConfig"`
		Storyboards struct {
			PlayerStoryboardSpecRenderer struct {
				Spec string `json:"spec"`
			} `json:"playerStoryboardSpecRenderer"`
		} `json:"storyboards"`
		Microformat struct {
			PlayerMicroformatRenderer struct {
				Thumbnail struct {
					Thumbnails []struct {
						URL    string `json:"url"`
						Width  int    `json:"width"`
						Height int    `json:"height"`
					} `json:"thumbnails"`
				} `json:"thumbnail"`
				Embed struct {
					IframeURL      string `json:"iframeUrl"`
					FlashURL       string `json:"flashUrl"`
					Width          int    `json:"width"`
					Height         int    `json:"height"`
					FlashSecureURL string `json:"flashSecureUrl"`
				} `json:"embed"`
				Title struct {
					SimpleText string `json:"simpleText"`
				} `json:"title"`
				Description struct {
					SimpleText string `json:"simpleText"`
				} `json:"description"`
				LengthSeconds        string   `json:"lengthSeconds"`
				OwnerProfileURL      string   `json:"ownerProfileUrl"`
				OwnerGplusProfileURL string   `json:"ownerGplusProfileUrl"`
				ExternalChannelID    string   `json:"externalChannelId"`
				AvailableCountries   []string `json:"availableCountries"`
				IsUnlisted           bool     `json:"isUnlisted"`
				HasYpcMetadata       bool     `json:"hasYpcMetadata"`
				ViewCount            string   `json:"viewCount"`
				Category             string   `json:"category"`
				PublishDate          string   `json:"publishDate"`
				OwnerChannelName     string   `json:"ownerChannelName"`
				UploadDate           string   `json:"uploadDate"`
			} `json:"playerMicroformatRenderer"`
		} `json:"microformat"`
		TrackingParams string `json:"trackingParams"`
		Attestation    struct {
			PlayerAttestationRenderer struct {
				Challenge    string `json:"challenge"`
				BotguardData struct {
					Program        string `json:"program"`
					InterpreterURL string `json:"interpreterUrl"`
				} `json:"botguardData"`
			} `json:"playerAttestationRenderer"`
		} `json:"attestation"`
		VideoQualityPromoSupportedRenderers struct {
			VideoQualityPromoRenderer struct {
				TriggerCriteria struct {
					ConnectionWhitelists   []string `json:"connectionWhitelists"`
					JoinLatencySeconds     int      `json:"joinLatencySeconds"`
					RebufferTimeSeconds    int      `json:"rebufferTimeSeconds"`
					WatchTimeWindowSeconds int      `json:"watchTimeWindowSeconds"`
					RefractorySeconds      int      `json:"refractorySeconds"`
				} `json:"triggerCriteria"`
				Text struct {
					Runs []struct {
						Text string `json:"text"`
						Bold bool   `json:"bold"`
					} `json:"runs"`
				} `json:"text"`
				Endpoint struct {
					ClickTrackingParams string `json:"clickTrackingParams"`
					URLEndpoint         struct {
						URL    string `json:"url"`
						Target string `json:"target"`
					} `json:"urlEndpoint"`
				} `json:"endpoint"`
				TrackingParams string `json:"trackingParams"`
				CloseButton    struct {
					VideoQualityPromoCloseRenderer struct {
						TrackingParams string `json:"trackingParams"`
					} `json:"videoQualityPromoCloseRenderer"`
				} `json:"closeButton"`
			} `json:"videoQualityPromoRenderer"`
		} `json:"videoQualityPromoSupportedRenderers"`
	} `json:"player_response"`
}

// New ... nothing fancy here
func New() Context {
	c := Context{}
	return c
}

// GetNewFromID ... Instanciates a NEW datat struct and sets the data inside based off the YT VIDEO ID it gets passed
func GetNewFromID(vID string) (Context, error) {
	c := New()
	err := c.SetByID(vID)
	return c, err
}

// GetNewFromURL ... Instanciates a NEW datat struct and sets the data inside based off the YT VIDEO URL it gets passed
func GetNewFromURL(ytURL string) (Context, error) {
	c := New()
	err := c.SetByURL(ytURL)
	return c, err
}

// SetByID ... Sets the data inside the data struct based off the YT VIDEO ID it gets passed
func (c *Context) SetByID(vID string) error {
	resp, err := http.Get("http://youtube.com/get_video_info?video_id=" + vID)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	bodyBytes, err := ioutil.ReadAll(resp.Body)
	bodyString := string(bodyBytes)

	videoInfoKindOfParsed, err := url.ParseQuery(bodyString)
	if err != nil {
		return err
	}

	for key, val := range videoInfoKindOfParsed {

		// Get the string val
		var stringVal string
		if len(val) > 0 {
			stringVal = val[0]
		}

		// Set the struct
		switch key {
		case "innertube_api_key":
			c.InnertubeAPIKey = stringVal
		case "gapi_hint_params":
			c.GAPIHintParams = stringVal
		case "host_language":
			c.HostLanguage = stringVal
		case "fmt_list":
			c.FMTList = stringVal
		case "vss_host":
			c.VSSHost = stringVal
		case "innertube_context_client_version":
			c.InnertubeContextClientVersion = stringVal
		case "innertube_api_version":
			c.InnertubeAPIVersion = stringVal
		case "enablecsi":
			c.EnableCSI = stringVal
		case "fexp":
			fexps := strings.Split(stringVal, ",")
			for _, fexp := range fexps {
				c.FExp = append(c.FExp, cast.ToInt64(fexp))
			}
		case "root_ve_type":
			c.RootVEType = stringVal
		case "watermark":
			c.Watermark = stringVal
		case "csi_page_type":
			c.CSIPageType = stringVal
		case "timestamp":
			c.Timestamp = cast.ToInt64(stringVal)
		case "fflags":
			c.FFlags = stringVal
		case "csn":
			c.CSN = stringVal
		case "c":
			c.C = stringVal
		case "url_encoded_fmt_stream_map":
			c.URLEncodedFMTStreamMap = stringVal
		case "hl":
			c.HL = stringVal
		case "cver":
			c.CVer = stringVal
		case "cr":
			c.CR = stringVal
		case "adaptive_fmts":
			c.AdaptiveFmts = stringVal
		case "player_response":
			err = json.Unmarshal([]byte(stringVal), &c.PlayerResponse)
			if err != nil {
				return err
			}
		}

	}

	return nil
}

// SetByURL ... Sets the data inside the data struct based off the YT VIDEO URL it gets passed
func (c *Context) SetByURL(ytURL string) error {
	vID, err := c.GetIDFromURL(ytURL)
	if err != nil {
		return err
	}
	return c.SetByID(vID)
}

// GetIDFromURL ... Grabs and returns the ID from the YT URL
func (c *Context) GetIDFromURL(ytURL string) (string, error) {
	// Parse the URL
	ytURLParsed, err := url.Parse(ytURL)
	if err != nil {
		return "", err
	}

	// Parse the Query
	ytQueryParsed, err := url.ParseQuery(ytURLParsed.RawQuery)
	if err != nil {
		return "", err
	}

	// Find the ID
	for key, val := range ytQueryParsed {
		if key == "v" && len(val) > 0 {
			if len(val[0]) > 0 {
				return val[0], nil
			}
		}
	}

	// If this is executing, then no id was found
	return "", Error.New("VIDEO ID NOT FOUND IN URL")
}

// GetBestDownloadOption ... Runs logic on different format arrays to find the best option for downloading this stream
func (c *Context) GetBestDownloadOption() FormatDecisionModel {

	// This is the model we will be returning with the desired data in it
	formatDecisionModel := FormatDecisionModel{}
	formatDecisionModel.Title = c.PlayerResponse.VideoDetails.Title

	// ** ADAPTIVE FORMATS: BEGIN ** //
	// We want to strive for the best quality first which would be adaptive formats
	adaptiveFormats := c.getBestAdaptiveFormat()

	// We REQUIRE 2 in this array as adapative A/V is seperate. Item 0 is the video stream, Item 1 is the audio stream
	if len(adaptiveFormats) == 2 {
		formatDecisionModel.UseAdaptiveFormatStreams = true
		formatDecisionModel.AdaptiveFormatStreams = adaptiveFormats
		return formatDecisionModel
	}
	// ** ADAPTIVE FORMATS: END ** //

	// ** REGULAR FORMATS: BEGIN ** //
	// Obviously if this is executing, we are looking at a secondary option as the adapive formats were a bust
	formats := c.getBestFormat()
	if len(formats) == 1 {
		formatDecisionModel.UseFormatStream = true
		formatDecisionModel.FormatStream = formats[0]
		return formatDecisionModel
	}
	// ** REGULAR FORMATS: END ** //

	// ** EMBEDED STREAM: BEGIN ** //
	// TODO: At some point we need to program this for videos that are crazy protected
	// TODO: This is the reverse engineered way of doing so  https://tyrrrz.me/blog/reverse-engineering-youtube
	formatDecisionModel.UseEmbededStream = true // Plug this in here just to satisfy the rest
	// ** EMBEDED STREAM: END ** //

	return formatDecisionModel
}

// getBestAdaptiveFormat ... Tries to get the best possible download option from Context.PlayerResponse.StreamingData.AdaptiveFormats
func (c *Context) getBestAdaptiveFormat() []AdaptiveFormat {
	var adaptiveVideoFormats []AdaptiveFormat
	var adaptiveAudioFormats []AdaptiveFormat

	fmt.Println("THERE ARE " + cast.ToString(len(c.PlayerResponse.StreamingData.AdaptiveFormats)) + " ADAPTIVE FORMATS")

	// Extract video and audio formats
	for _, adaptiveFormat := range c.PlayerResponse.StreamingData.AdaptiveFormats {

		// Is Video Only, Else it is audio only
		if len(adaptiveFormat.AudioQuality) == 0 &&
			len(adaptiveFormat.AudioSampleRate) == 0 {
			adaptiveVideoFormats = append(adaptiveVideoFormats, adaptiveFormat)
		} else {
			adaptiveAudioFormats = append(adaptiveAudioFormats, adaptiveFormat)
		}

	}

	// Bubble Sort VIDEO By Average Bitrate
	adaptiveVideoFormats = c.bubbleSortAdaptiveFormatsByAvgBr(adaptiveVideoFormats)
	// Bubble Sort AUDIO By Average Bitrate
	adaptiveAudioFormats = c.bubbleSortAdaptiveFormatsByAvgBr(adaptiveAudioFormats)

	var returns []AdaptiveFormat

	// Add the first adaptive video format to the returns
	if len(adaptiveVideoFormats) > 0 {
		returns = append(returns, adaptiveVideoFormats[0])
	}

	// Add the first adaptive audio format to the returns
	if len(adaptiveAudioFormats) > 0 {
		returns = append(returns, adaptiveAudioFormats[0])
	}

	return returns
}

// getBestFormat ... Tries to get the best possible download option from Context.PlayerResponse.StreamingData.Formats
func (c *Context) getBestFormat() []Format {

	fmt.Println("THERE ARE " + cast.ToString(len(c.PlayerResponse.StreamingData.Formats)) + " STANDARD FORMATS")

	// Bubble Sort By Average Bitrate
	formats := c.bubbleSortFormatsByAvgBr(c.PlayerResponse.StreamingData.Formats)

	var returns []Format

	// Add the first format to the returns
	if len(formats) > 0 {
		returns = append(returns, formats[0])
	}

	return returns
}

// bubbleSortAdaptiveFormatsByAvgBr ... simpley bubble sorts an array of AdaptiveFormat type by average bitrate
func (c *Context) bubbleSortAdaptiveFormatsByAvgBr(adaptiveFormats []AdaptiveFormat) []AdaptiveFormat {
	if len(adaptiveFormats) > 1 {
		for i := 0; i < len(adaptiveFormats); i++ {
			for j := 0; j < len(adaptiveFormats)-1-i; j++ {
				if adaptiveFormats[j].AverageBitrate < adaptiveFormats[j+1].AverageBitrate {
					adaptiveFormats[j], adaptiveFormats[j+1] = adaptiveFormats[j+1], adaptiveFormats[j]
				}
			}
		}
	}
	return adaptiveFormats
}

// bubbleSortFormatsByAvgBr ... simpley bubble sorts an array of Format type by average bitrate
func (c *Context) bubbleSortFormatsByAvgBr(formats []Format) []Format {
	if len(formats) > 1 {
		for i := 0; i < len(formats); i++ {
			for j := 0; j < len(formats)-1-i; j++ {
				if formats[j].AverageBitrate < formats[j+1].AverageBitrate {
					formats[j], formats[j+1] = formats[j+1], formats[j]
				}
			}
		}
	}
	return formats
}
